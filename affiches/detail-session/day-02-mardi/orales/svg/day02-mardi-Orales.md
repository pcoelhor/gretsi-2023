# Mardi 29-08 - Orales

-- Auditorium

08:30 Conférence plénière : Gersende Fort
10:00 Session Orale : Traitement multi-capteurs
15:40 Session Orale : TSI et IA pour l’environnement
08:30 Conférence plénière : Isabelle Collet

-- Chrome 1

10:00 Session Orale : Adéquation algorithmes et architectures
15:40 Session Orale : TS et applications en mécanique

-- Titane 2

10:00 Session Orale : Apprentissage et allocation de ressources
15:30 Table Ronde : Enseignement et didactique du TSI