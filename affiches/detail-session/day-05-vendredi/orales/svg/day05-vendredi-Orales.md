# Jeudi 31-08 - Orales

-- Auditorium

08:30 Session Orale : Apprentissage sur graphe et apprentissage de graphe
10:30 Session Orale : Imagerie computationnelle

-- Chrome 1

08:30 Session Orale : Décompositions matricielles tensorielles sous contraintes
10:30 Session Orale : Modèles et apprentissage

-- Titane 2

08:30 Session Orale : Enjeux Sociaux et Éthiques
10:30 Session Orale : Théorie de l’information et test d’hypothèses
