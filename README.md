# GRETSI 2023

Ceci est un repo pour stocker plusieurs fichiers qui ont été utiles dans la
préparation de l'édition 2023 du GRETSI à Grenoble. 

- Dans `/affiches` on trouve les fichiers `svg` de toutes le affiches du
colloque. Cela inclut les affiches A0 avec le résumé de la journée, les affiches
A3 mises à l'entrée de chaque salle, la banderole mise à l'exterieur de
l'immeuble avec le logo GRETSI 2023, etc.

- Dans `/badges` on trouve le workflow proposé par [Cléo Baras](https://ttk.gricad-gitlab.univ-grenoble-alpes.fr/barasc) pour générer
les badges des participants de l'événement.

- Dans `/fonts` on trouve les polices Roboto et Rajdhani qui ont été utilisées
partout dans les éléments visuels du colloque

- Dans `/logos` on trouve les fichiers vectoriels `svg` de plusieurs sponsors
nationaux qui ont sponsorisé le colloque. Les logos des organismes locaux ont
été supprimés.

### Quelques commentaires

- Lorsqu'on ouvre le logo GRETSI dans `Inkscape` on peut (fichier `/logos/gretsi.svg`)
éditer le mot "GRETSI" pour ajouter l'année du colloque. Cela nous permet de
garder les choses avec la même police. En cas de doute, la police s'appelle
[PT Sans](https://fonts.google.com/specimen/PT+Sans) en format bold.

- La palette de couleurs du GRETSI 2023 était:
  - Lundi: #39B385
  - Mardi: #F1656A
  - Mercredi: #8D6AA0
  - Jeudi: #DD9C27
  - Vendredi: #3572B9