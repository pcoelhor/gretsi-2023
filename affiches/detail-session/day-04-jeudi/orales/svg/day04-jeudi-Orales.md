# Jeudi 31-08 - Orales

-- Auditorium

08:30 Session Orale : Apprentissage sur graphe et apprentissage de graphe
13:45 Conférence plénière : Philippe Ciuciu
17:00 Initiative organisateurs

-- Chrome 1

08:30 Session Orale : Décompositions matricielles et tensorielles sous contraintes

-- Titane 2

08:30 Session Orale : Implémentation matérielle d’algorithmes d’IA