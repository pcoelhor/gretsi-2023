# Lundi 28-08 - Orales

-- Auditorium

13:00 Accueil
14:00 Conférence plénière Wendy Mackay
17:30 Session Orale : Inférence statistique, échantillonnage

-- Chrome 1

17:30 Session Orale : Apprentissage profond

-- Titane 2

17:30 Session Orale : Bio-ingénerie et Sciences de la vie