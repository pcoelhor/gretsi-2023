# Mercredi 30-08 - Orales

-- Auditorium

08:30 Session Orale : Histoire et évolution de la discipline
13:30 Conférence prestige : A. M. Lagrange
14:50 Session Orale : Graphes, parcimonie et optimisation
16:30 Prix de thèse

-- Chrome 1

08:30 Session Orale : Apprentissage et codages
14:50 Session Orale : Applications en TS et imagerie

-- Titane 2

08:30 Session Orale : Problèmes inverses
14:50 Session Orale : Décision et estimation statistique